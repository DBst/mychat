// react
import * as React from "react";
// style
import './WelcomeView.css';
import banner from '../images/700x350.jpg';
// material
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import {Link} from "react-router-dom";

const WelcomeView = () => {

    return (
        <div>
            <div className={'content text-light'}>
                <div className={'pt-5 container'}>
                    <div className={'pt-5 row'}>
                        <div className={'col-md-6 pt-5'}>
                            <h5>
                                An app for chatting with strangers
                            </h5>
                            <h1 className={'position-relative fw-bold'} style={{top: '-10px'}}>
                                My Chat
                            </h1>
                            <Stack direction="row" spacing={2}>
                                <Link to={'/chat'}>
                                    <Button color={'inherit'} className={'text-primary'} variant="contained">
                                        Start chatting
                                    </Button>
                                </Link>
                                <Button color={'info'} className={'text-light'} variant="contained">
                                    Change language
                                </Button>
                            </Stack>
                        </div>
                        <div className={'col-md-6 d-none d-md-block'}>
                            <img src={banner} className={'w-100 banner-image position-relative shadow rounded'} alt="banner" />
                        </div>
                    </div>
                </div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#0099ff" fillOpacity="1" d="M0,288L40,266.7C80,245,160,203,240,176C320,149,400,139,480,138.7C560,139,640,149,720,154.7C800,160,880,160,960,133.3C1040,107,1120,53,1200,26.7C1280,0,1360,0,1400,0L1440,0L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path></svg>
            <div className={'container'}>
                <div className={'row'}>
                    <div className={'col-md-12 text-center'}>
                        Work in progress...
                    </div>
                </div>
            </div>
        </div>
    );
}

export default WelcomeView;