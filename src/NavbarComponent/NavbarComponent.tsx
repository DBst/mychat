// react
import React, {useEffect, useState} from "react";
// bootstrap elements
import { Navbar, Container, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap';
// style
import './NavbarComponent.css';
import {Link} from "react-router-dom";
import firebase from "firebase/compat/app";

const NavbarCompontent = () => {

    const currentPage = window.location.pathname;

    const [isSignedIn, setIsSignedIn] = useState(false); // Local signed-in state.

    // Listen to the Firebase Auth state and set the local state.
    useEffect(() => {
        const unregisterAuthObserver = firebase.auth().onAuthStateChanged(user => {
            setIsSignedIn(!!user);
        });
        return () => unregisterAuthObserver(); // Make sure we un-register Firebase observers when the component unmounts.
    }, []);

    return (
        <div className={'position-relative'}>
            <Navbar variant={'dark'} expand={'lg'}>
                <Container>
                    <Link to={'/'} className={'navbar-brand'}>
                        MyChat
                    </Link>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                        <Nav className="ms-auto my-2 my-lg-0" activeKey={currentPage}>
                            <Link to={'/'} className={'nav-link'}>
                                Home
                            </Link>
                            {isSignedIn ? (
                                <Link to={'/myaccount'} className={'nav-link'}>
                                    My account
                                </Link>
                            ) : (
                                <Link to={'/signin'} className={'nav-link'}>
                                    Sign in
                                </Link>
                            )}
                            <NavDropdown title="Options" id="navbarScrollingDropdown">
                                <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
                                <NavDropdown.Item href="#action4">Another action</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item href="#action5">
                                    Something else here
                                </NavDropdown.Item>
                            </NavDropdown>
                            <Nav.Link href="#" disabled>
                                Link
                            </Nav.Link>
                            {isSignedIn && (
                                <Link to={'/'} onClick={() => firebase.auth().signOut()} className={'nav-link'}>
                                    Logout
                                </Link>
                            )}
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default NavbarCompontent;