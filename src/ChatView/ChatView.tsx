import React, {useRef, useState, useReducer, useEffect} from "react";
// firebase
import {doc, getDocs, setDoc, updateDoc, deleteDoc, getFirestore, arrayUnion, onSnapshot, collection, query, where, limit} from "firebase/firestore";
import firebase from "firebase/compat/app";
//bootstrap elements
import { Form } from 'react-bootstrap';
// emoji
import { Picker } from 'emoji-mart/dist-es'
// style
import './ChatView.css';
import 'emoji-mart/css/emoji-mart.css'
// material
import InsertEmoticonIcon from '@mui/icons-material/InsertEmoticon';
import SendIcon from '@mui/icons-material/Send';
import {Button, IconButton} from '@mui/material';
// moment js
import Moment from "react-moment";

const ChatView = () => {

    // variables
    const userID = useRef('');
    const strangerID = useRef('');
    const chatID = useRef('');
    const chatStatus = useRef(0); //0 - first state, 1 - connecting to servers, 2 - finding strangers, 3 - successfully finding a stranger & start chat, 4 - ending the chat
    const messages = useRef([]);
    const [showEmoji, setShowEmoji] = useState(false);
    const [chatBottom, setChatBottom] = useState(true);
    const [text, setText] = useState('');
    const [ignored, forceUpdate] = useReducer(x => x + 1, 0); //forcing site update after getting new messages etc.

    const db = getFirestore();

    //Generate userID
    useEffect(() => {
        firebase.auth().onAuthStateChanged(user => {
            if (!user) {
                if(userID.current === "") {
                    let genUserID = "rand_" + getRandomInt(10000000, 99999999).toString() + Math.floor(Date.now()).toString();
                    userID.current = genUserID;
                    forceUpdate();
                }
            } else if (user.uid !== userID.current) {
                userID.current = user.uid;
                forceUpdate();
            }
        });
    });

    const getRandomInt = (min: number, max: number): number => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    //listen for changes in status and update messages
    const chatListener = () => {
        const unsubChat = onSnapshot(doc(db, 'chat', chatID.current), (doc) => {
            let data = doc.data();
            if(typeof data != 'undefined') {
                messages.current = data.messages;
                forceUpdate();
                if(chatBottom) {
                    scrollToChatBottom();
                }
                if(data.status !== chatStatus.current) {
                    chatStatus.current = data.status;
                    forceUpdate();
                }
                if(data.status === 4) {
                    unsubChat();
                }
            }
        });
    }

    const addMessage = (message: string) => {
        let time = Math.floor(Date.now());
        return updateDoc(doc(db, 'chat', chatID.current), {
            messages: arrayUnion({
                user_id: userID.current,
                value: message,
                added_at: time,
            }),
            updated_at: time
        });
    }

    const sendMessage = () => {
        let message = document.getElementById('message-field') as HTMLInputElement;
        if(message.value.length) {
            addMessage(message.value)!.then((e) => {
                setText('');
                message.value = '';
            });
        }
    }

    const startChat = () => {
        if(userID.current) {
            chatStatus.current = 1;
            strangerID.current = '';
            messages.current = [];
            forceUpdate();
            let query_ = query(collection(db, 'waiting-room'), where('status', '==', 0), limit(1));
            //find someone who is looking for a chat, if there is no one, add a record
            getDocs(query_).then((response) => {
                if (!response.empty) {
                    response.forEach((item) => {
                        prepareChatWithStranger(item);
                    });
                } else {
                    addToWaitingRoom();
                }
            })
        }
    }

    //get a user from db and start a chat with him
    const prepareChatWithStranger = (item: any) => {
        strangerID.current = item.data().user_id;
        updateDoc(doc(db, 'waiting-room', strangerID.current), {
            status: 2,
            user_id: strangerID.current!,
            stranger_id: userID.current,
        }).then((e) => {
            chatStatus.current = 2;
            forceUpdate();
            listenWaitingRoomStatus(strangerID.current);
        });
    }

    //adding a user to the waiting room
    const addToWaitingRoom = () => {
        setDoc(doc(db, 'waiting-room', userID.current), {
            status: 0,
            user_id: userID.current,
            stranger_id: null,
        }).then((e) => {
            chatStatus.current = 2;
            forceUpdate();
            const unsubscribe = onSnapshot(doc(db, 'waiting-room', userID.current), (response) => {
                let stranger = response.data();
                if (typeof stranger != 'undefined' && stranger.status == 2) {
                    strangerID.current = stranger.stranger_id

                    //creating unique chat ID
                    chatID.current = Math.floor(Date.now()) + '|' + userID.current + '|' + strangerID.current + '|' + getRandomInt(1000000, 9999999);
                    updateDoc(doc(db, 'waiting-room', userID.current), {
                        status: 3,
                        user_id: userID.current,
                        stranger_id: stranger.stranger_id!,
                        chat_id: chatID.current,
                    }).then((e) => {
                        forceUpdate();
                        unsubscribe();
                        listenWaitingRoomStatus(userID.current);
                    });
                }
            });
        });
    }

    const endChat = () => {
        let time = Math.floor(Date.now());
        updateDoc(doc(db, 'chat', chatID.current), {
            updated_at: time,
            status: 4,
        }).then(() => {
            forceUpdate();
        });
        chatStatus.current = 4;
        return true;
    }

    const listenWaitingRoomStatus = (chatOwnerID: string) => {
        const unsubscribe = onSnapshot(doc(db, 'waiting-room', chatOwnerID!), (response) => {
            let chatData = response.data();
            if (typeof chatData != 'undefined') {
                //update chat status
                chatStatus.current = chatData.status;
                forceUpdate();
                if(chatData.status == 3) {
                    chatID.current = chatData.chat_id;
                    chatListener();
                    unsubscribe();
                    if(chatOwnerID! == strangerID.current) {
                        deleteDoc(doc(db, 'waiting-room', chatOwnerID!)).then(() => {
                            let time = Math.floor(Date.now());
                            setDoc(doc(db, 'chat', chatID.current), {
                                updated_at: time,
                                messages: [],
                                status: 3,
                            });
                        });
                    }
                }
            }
        });
    }

    const scrollToChatBottom = () => {
        let el = document.getElementById('messages');
        if(el !== null) {
            el.scrollTo(0, 999999);
        }
    }

    const handleEmojiShow = () => {
        setShowEmoji((v) => !v)
    }
    const handleEmojiSelect = (e:any) => {
        setText((text) => (text += e.native))
    }

    function handlePressEnter (e: any) {
        e.preventDefault();
        if(chatStatus.current == 3) {
            sendMessage();
        }
    }

    const handleChangeText = (e:any) => {
        setText(e.target.value)
    }

    const handleChatScroll = (e:any) => {
        let target = e.target;
        setChatBottom(false);
        if(target.scrollTopMax === target.scrollTop) {
            setChatBottom(true);
        }
    }

    const Messages = (messages: any): any => {
        if(messages.messages.length) {
            return messages.messages.map((message: any, i: number) =>
                <div key={i} className={'w-100 d-table'}>
                    {message.user_id == userID.current ? (
                        <div className="message message-you">
                            <p className="message-content">{message.value}</p>
                            <div className="message-timestamp text-muted">
                                <Moment format={'HH:mm:ss'}>
                                    {message.added_at}
                                </Moment>
                            </div>
                        </div>) : (
                        <div className="message message-stranger">
                            <p className="message-content">{message.value}</p>
                            <div className="message-timestamp text-muted">
                                <Moment format={'HH:mm:ss'}>
                                    {message.added_at}
                                </Moment>
                            </div>
                        </div>
                    )}
                </div>
            )
        }
        return (
            <div>
                <p className={'text-muted mt-3 ms-3'} style={{display: chatStatus.current == 0 || chatStatus.current == 4 ? 'unset' : 'none'}}>
                    Click on the "Start chat" button to start the conversation.
                </p>
                <p className={'text-muted mt-3 ms-3'} style={{display: chatStatus.current == 1 ? 'unset' : 'none'}}>
                    Connecting to servers...
                </p>
                <p className={'text-muted mt-3 ms-3'} style={{display: chatStatus.current == 2 ? 'unset' : 'none'}}>
                    We are looking for a free stranger, give us a moment...
                </p>
                <p className={'text-muted mt-3 ms-3'} style={{display: chatStatus.current == 3 ? 'unset' : 'none'}}>
                    Say hello to the stranger
                </p>
            </div>
        )
    };

    return (
        <div>
            <div className={'content'}>
                <div className="container pt-5">
                    <div className={'row justify-content-center pt-5'}>
                        <div className={'col-md-8 p-3 chat-content'}>

                            <div id={'messages'} onScroll={handleChatScroll}>
                                <Messages messages={messages.current} />
                            </div>
                            <div id={'chat-status'}>
                                <div id={'user-end-chat'} className={'alert-danger alert p-2'} style={{display: chatStatus.current == 4 ? 'block' : 'none'}}>
                                    You or the stranger have ended the conversation
                                </div>
                            </div>

                            <div className={'chat-bottom-content'}>
                                <div className={'container-fluid p-0 mt-3'}>
                                    <div className={'row'}>
                                        <div className={'col-1 text-center p-0'}>
                                            <IconButton type='button' onClick={handleEmojiShow}>
                                                <InsertEmoticonIcon></InsertEmoticonIcon>
                                            </IconButton>
                                        </div>
                                        <div className={'col-10'}>
                                            <Form onSubmit={handlePressEnter}>
                                            <Form.Control
                                                id={'message-field'}
                                                value={text}
                                                onChange={handleChangeText}
                                                onDragEnter={handlePressEnter}
                                                type='text'
                                                placeholder='Type message...' />
                                            </Form>
                                        </div>
                                        <div className={'col-1 text-center p-0'} style={{marginLeft: '-10px'}}>
                                            <IconButton type='button' onClick={sendMessage} disabled={chatStatus.current !== 3}>
                                                <SendIcon></SendIcon>
                                            </IconButton>
                                        </div>
                                    </div>
                                </div>
                                <div className={'position-relative'}>
                                    <div className={'emoji-content'}>
                                        {showEmoji && <Picker onSelect={handleEmojiSelect} emojiSize={20} />}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={'col-md-8'}>
                            <div className={'container-fluid p-0'}>
                                <div className={'row justify-content-end mt-2'} id={'buttons'}>
                                    <div className={'col-md-2 save-chat mt-2'} style={{display: chatStatus.current == 3 ? 'unset' : 'none'}}>
                                        <Button variant={'contained'} color={'success'} className={'w-100'} disabled={true}>
                                            Save chat
                                        </Button>
                                    </div>
                                    <div className={'col-md-3 end-chat mt-2'} style={{display: chatStatus.current == 3 ? 'unset' : 'none'}}>
                                        <Button variant={'contained'} color={'error'} className={'w-100'} onClick={endChat}>
                                            End chat
                                        </Button>
                                    </div>
                                    <div className={'col-md-3 start-chat mt-2'} style={{display: chatStatus.current == 0 || chatStatus.current == 4 ? 'unset' : 'none'}}>
                                        <Button variant={'contained'} color={'info'} className={'w-100'} onClick={startChat} disabled={userID.current == "" ? true : false}>
                                            Start chat
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <svg className={'wave'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#0099ff" fillOpacity="1" d="M0,288L40,266.7C80,245,160,203,240,176C320,149,400,139,480,138.7C560,139,640,149,720,154.7C800,160,880,160,960,133.3C1040,107,1120,53,1200,26.7C1280,0,1360,0,1400,0L1440,0L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path></svg>
        </div>
    );
}

export default ChatView;