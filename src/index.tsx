import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';

// notifications
import { SnackbarProvider } from 'notistack';

//firebase
import firebase from 'firebase/compat/app';
import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useCollectionData } from 'react-firebase-hooks/firestore';
firebase.initializeApp({
    apiKey: "AIzaSyBAtWm0FJEWUAqbVh_stnrx27Xpp8A8By0",
    authDomain: "mychat-bdcea.firebaseapp.com",
    projectId: "mychat-bdcea",
    storageBucket: "mychat-bdcea.appspot.com",
    messagingSenderId: "950125496064",
    appId: "1:950125496064:web:af06db98960967454e95e5",
    measurementId: "G-0GNTXHS93Q"
});

ReactDOM.render(
    <React.StrictMode>
        <SnackbarProvider maxSnack={3}>
            <App />
        </SnackbarProvider>
    </React.StrictMode>
,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();