// react
import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
// views
import ChatView from "./ChatView/ChatView";
import WelcomeView from "./WelcomeView/WelcomeView";
import SignIn from "./SignIn/SignIn";
// components
import NavbarComponent from "./NavbarComponent/NavbarComponent";

const App = () => {
    return (
        <div className={'App'}>
            <BrowserRouter>
                <NavbarComponent/>
                <Routes>
                    <Route path="/" element={<WelcomeView />} />
                    <Route path="/chat" element={<ChatView />} />
                    <Route path="/signin" element={<SignIn />} />
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;